package com.example.net.pocketcard.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Comment;
import com.example.net.pocketcard.R;

import java.util.List;

/**
 * Created by PrangerZ54 on 3/31/2016 AD.
 */
public class CommentListAdapter extends ArrayAdapter<Comment>{

    public CommentListAdapter(Context context, int resource, List<Comment> objects){
        super(context,resource,objects);
    }

    public View getView(int position, View convertView, ViewGroup parent){

        View v = convertView;

        if(v == null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.comment_cell, null);
        }

        TextView NewCard = (TextView) v.findViewById(R.id.commenttext);

        Comment comment = getItem(position);
        NewCard.setText(comment.getText());

        return v;

    }

}
