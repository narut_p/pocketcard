package com.example.net.pocketcard.Models;

import java.io.Serializable;

/**
 * Created by PrangerZ54 on 3/31/2016 AD.
 */
public class Comment implements Serializable{

    private String text;

    public Comment(String text){

        this.text = text;

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String toString(){
        return "Text:" + text;
    }



}
