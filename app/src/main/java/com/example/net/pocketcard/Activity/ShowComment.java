package com.example.net.pocketcard.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.net.pocketcard.Adapter.CommentListAdapter;
import com.example.net.pocketcard.Models.Card;
import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Comment;
import com.example.net.pocketcard.Models.Storage;
import com.example.net.pocketcard.R;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PrangerZ54 on 3/31/2016 AD.
 */
public class ShowComment extends AppCompatActivity {

    int indexcard, indexlist;

    private CardList cardList;
    private Card card;
    private Comment comment;
    private ListView showComment;
    private CommentListAdapter commentListAdapter;
    private List<Comment> comments;

    private Button deleteButton;
    private Button notDeleteButton;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_comment);
        indexlist = this.getIntent().getIntExtra("i", -1);
        indexcard = this.getIntent().getIntExtra("j", -1);
        card = Storage.getInstance().loadSavedCard().get(indexlist).getNewCardList().get(indexcard);
        initComponents();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }


    private void initComponents() {
        comments = new ArrayList<Comment>();
        showComment = (ListView) findViewById(R.id.showCommentListView);
        commentListAdapter = new CommentListAdapter(this, R.layout.comment_cell, comments);
        showComment.setAdapter(commentListAdapter);
        showComment.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                //custom dialog
                final Dialog dialogDel = new Dialog(ShowComment.this);
                dialogDel.setContentView(R.layout.delete_comment_dialog);
                dialogDel.setTitle("Do you want to delete");

                //set a dialog
                deleteButton = (Button) dialogDel.findViewById(R.id.deleteBtn);

                notDeleteButton = (Button) dialogDel.findViewById(R.id.notDeleteCommentBtn);


                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Storage.getInstance().loadSavedCard().get(indexlist).getNewCardList().get(indexcard).getComments().remove(position);
                        finish();
                    }
                });

                notDeleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogDel.dismiss();
                    }
                });


                dialogDel.show();
                return true;
            }
        });

        loadCommentList();


    }


    private void loadCommentList() {
        comments.clear();
        for (Comment n : Storage.getInstance().loadSavedCard().get(indexlist).getNewCardList().get(indexcard).getComments()) {
            comments.add(n);
        }
        commentListAdapter.notifyDataSetChanged();
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onPostResume() {
        super.onPostResume();
        loadCommentList();
    }


}
