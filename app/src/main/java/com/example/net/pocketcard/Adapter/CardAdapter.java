package com.example.net.pocketcard.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ArrayAdapter;

import com.example.net.pocketcard.Models.Card;
import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.R;


import java.util.List;

/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class CardAdapter extends ArrayAdapter<Card> {

    public CardAdapter(Context context, int resource, List<Card> objects){
        super(context,resource,objects);
    }

    public View getView(int position, View convertView, ViewGroup parent){

        View v = convertView;

        if(v == null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.card_cell, null);
        }

        TextView NewCard = (TextView) v.findViewById(R.id.cardCell);

        Card card = getItem(position);
        NewCard.setText(card.getCardTitle());
        if(card.getColor().equals("Green")) NewCard.setBackgroundColor(Color.GREEN);
        else if(card.getColor().equals("Red")) NewCard.setBackgroundColor(Color.RED);
        else if(card.getColor().equals("Yellow")) NewCard.setBackgroundColor(Color.YELLOW);
        return v;

    }
}
