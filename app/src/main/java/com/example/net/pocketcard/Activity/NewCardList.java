package com.example.net.pocketcard.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Storage;
import com.example.net.pocketcard.R;

public class NewCardList extends AppCompatActivity {

    private Button SaveListButton;
    private Button BackToMainButton;
    private TextView CardNameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_card_list);
        initComponents();
    }

    private void initComponents() {
        BackToMainButton = (Button)findViewById(R.id.cancelNewListBtn);
        BackToMainButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent intent = new Intent(NewCardList.this,MainActivity.class);
                startActivity(intent);
            }
        });

        CardNameList = (TextView)findViewById(R.id.newListNameTxt);


        SaveListButton = (Button)findViewById(R.id.saveListBtn);
        SaveListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            //SavedNote
            public void onClick(View view) {
                Intent intent = new Intent(NewCardList.this,MainActivity.class);
                Storage.getInstance().saveCardList(new CardList(CardNameList.getText().toString()));
                setResult(RESULT_OK,intent);
                finish();
            }
        });

    }



}
