package com.example.net.pocketcard.Models;

import android.graphics.Color;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class Card implements Serializable{

    private String title, description;
    private List<Comment> comments;
    private String color;

    public Card(String title, String description) {
        this.title = title;
        this.description = description;
        comments = new ArrayList<Comment>();
    }

    public String getCardTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setDesc(String description) {
        this.description = description;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments){
        this.comments = comments;
    }

    public void setColor(String color) {this.color = color;}

    public String getColor() {
        return color;
    }

    public String getCardDesc() {
        return description;
    }

    public String toString(){
        return "Title: " + title + " Description : " + description;
    }


}
