package com.example.net.pocketcard.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class CardList implements Serializable{

    private List<Card> NewCard;
    private String CardTitle;



    public CardList(String CardTitle){

        this.CardTitle = CardTitle;
        this.NewCard = new ArrayList<Card>();
    }

    public void setCardListTitle(String cardTitle){
        this.CardTitle = cardTitle;
    }

    public String getCardListTitle(){
        return CardTitle;
    }

    public List<Card> getNewCardList(){
        return NewCard;
    }


    public void addCardList(Card card) {
        NewCard.add(card);
    }
    public void clear(){
        NewCard.clear();
    }
    public void removeCard(int index){
        NewCard.remove(index);
    }
    public String toString(){
        return "CardTitle: "+ CardTitle;
    }

}
