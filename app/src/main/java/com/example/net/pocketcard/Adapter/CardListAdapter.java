package com.example.net.pocketcard.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.R;

import java.util.List;

/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class CardListAdapter extends ArrayAdapter<CardList> {

    public CardListAdapter(Context context, int resource, List<CardList> objects){
        super(context,resource,objects);
    }

    public View getView(int position, View convertView, ViewGroup parent){

        View v = convertView;

        if(v == null){
            LayoutInflater vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.cardlist_cell, null);
        }

        TextView NewCard = (TextView) v.findViewById(R.id.listCell);

        CardList card = getItem(position);
        NewCard.setText(card.getCardListTitle());

        return v;

    }


}
