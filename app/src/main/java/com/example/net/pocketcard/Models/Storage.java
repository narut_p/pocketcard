package com.example.net.pocketcard.Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class Storage {
    private List<CardList> SavedCard;
    private static Storage instance;

    private Storage() {
        SavedCard = new ArrayList<CardList>();
    }

    public static Storage getInstance(){

        if(instance==null){
            instance = new Storage();
        }
        return instance;
    }


    public void saveCardList(CardList card){

        SavedCard.add(card);
    }

    public List<CardList> loadSavedCard(){

        return SavedCard;
    }

    public void clearWholeList(){
        SavedCard.clear();
    }

   public void deleteCardList(int index){

        SavedCard.remove(index);

    }
}
