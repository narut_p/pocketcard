package com.example.net.pocketcard.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.net.pocketcard.Adapter.CardListAdapter;
import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Storage;
import com.example.net.pocketcard.R;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<CardList> cardList;
    private Button addCardList;
    private Button clearAllList;
    private CardListAdapter CardListAdapter;
    private ListView CardListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    private void initComponents() {
        addCardList = (Button)findViewById(R.id.addCardListBtn);
        addCardList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewCardList.class);
                startActivity(intent);
            }
        });

        cardList = new ArrayList<CardList>();
        CardListAdapter = new CardListAdapter(this,R.layout.cardlist_cell,cardList);
        CardListView = (ListView) findViewById(R.id.listListView);
        CardListView.setAdapter( CardListAdapter );
        CardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, ShowCardList.class);
                intent.putExtra("i", i);
                startActivity(intent);
            }
        });

        clearAllList = (Button)findViewById(R.id.clearAllListBtn);
        clearAllList.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Storage.getInstance().clearWholeList();
                onPostResume();

            }
        });






    }

    private void loadCardList() {
        cardList.clear();
        for (CardList n : Storage.getInstance().loadSavedCard()) {
            cardList.add(n);
        }
        CardListAdapter.notifyDataSetChanged();
    }

    protected void onStart(){
        super.onStart();
        Log.i("hello",Storage.getInstance().loadSavedCard().size()+"");
        cardList.clear();
        for(CardList cardlist: Storage.getInstance().loadSavedCard()) {
            cardList.add(cardlist);
        }
        CardListAdapter.notifyDataSetChanged();

    }

    protected void onPostResume(){
        super.onPostResume();
        loadCardList();
    }



}
