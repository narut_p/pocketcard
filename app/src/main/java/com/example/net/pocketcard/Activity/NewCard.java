package com.example.net.pocketcard.Activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.net.pocketcard.Models.Card;
import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Storage;
import com.example.net.pocketcard.R;

import java.util.List;


/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class NewCard extends AppCompatActivity {

    //Edit text
    private EditText cardTitle,cardDesc;
    private Spinner colorSpinner;


    private Button saveNewCard;
    private int listNo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_card);
        listNo = getIntent().getIntExtra("listNo", -1);
        initComponents();
    }

    private void initComponents() {
        saveNewCard = (Button) findViewById(R.id.saveNewCardBtn);
        cardTitle = (EditText) findViewById(R.id.newCardTitle);
        cardDesc = (EditText) findViewById(R.id.newCardDesc);
        colorSpinner = (Spinner) findViewById(R.id.colorSpinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.colors_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSpinner.setAdapter(spinnerAdapter);
        saveNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewCard.this, ShowCardList.class);
                Card newCard = new Card(cardTitle.getText().toString(), cardDesc.getText().toString());
                String selectColor = (String)colorSpinner.getSelectedItem();
                newCard.setColor(selectColor);
                Storage.getInstance().loadSavedCard().get(listNo).addCardList(newCard);
                Log.e("NewCard", "List No = " + listNo);
                Log.e("NewCard", "New Card is " + newCard.toString());
                for(Card card : Storage.getInstance().loadSavedCard().get(listNo).getNewCardList()) {
                    Log.e("NewCard", card.toString() );
                }
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
