package com.example.net.pocketcard.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.net.pocketcard.Models.Card;
import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Comment;
import com.example.net.pocketcard.Models.Storage;
import com.example.net.pocketcard.R;

import java.util.List;

public class ShowCard extends AppCompatActivity {
    private int indexList, indexCard;
    private Card thisCard;
    private CardList cardLists;
    //Button
    private Button renameCardTitle;
    private Button renameCardDes;
    private Button okRenameCardTitle;
    private Button okRenameCardDes;
    private Button okComment;
    private Button deleteCard;
    private Button addComment;
    private Button showComment;

    private EditText editCardTitle;
    private EditText editCardDes;
    private EditText editComment;
    //Textview
    private TextView showCardTitleTxt, showCardDescTxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_card);
        //Receive index of information
        indexList = this.getIntent().getIntExtra("i", 0);
        indexCard = this.getIntent().getIntExtra("j", 0);
        thisCard = Storage.getInstance().loadSavedCard().get(indexList).getNewCardList().get(indexCard);
        Log.i("hello2",indexList+"");
        Log.i("hello2",indexCard+"");

        initComponents();
    }
    private void initComponents() {
        showCardTitleTxt = (TextView)findViewById(R.id.showCardTitleTxt);
        showCardDescTxt = (TextView)findViewById(R.id.showCardDescTxt);

        showCardTitleTxt.setText(thisCard.getCardTitle());
        showCardDescTxt.setText(thisCard.getCardDesc());

        //Add button listener
        renameCardTitle = (Button)findViewById(R.id.renameCardTitleBtn);
        renameCardTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //custom dialog
                final Dialog dialogTitle = new Dialog(ShowCard.this);
                dialogTitle.setContentView(R.layout.rename_card_title_dialog);
                dialogTitle.setTitle("Rename card title");

                //set a dialog
                editCardTitle = (EditText) dialogTitle.findViewById(R.id.renameCardTitleTxt);

                okRenameCardTitle = (Button) dialogTitle.findViewById(R.id.okRenameCardTitleBtn);

                //when click ok rename
                okRenameCardTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        thisCard.setTitle(editCardTitle.getText().toString());
                        dialogTitle.dismiss();
                    }
                });
                dialogTitle.show();

            }
        });

        renameCardDes = (Button)findViewById(R.id.renameCardDesBtn);
        renameCardDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //custom dialog
                final Dialog dialogDes = new Dialog(ShowCard.this);
                dialogDes.setContentView(R.layout.rename_card_des_dialog);
                dialogDes.setTitle("Edit card description");

                //set a dialog
                editCardDes = (EditText) dialogDes.findViewById(R.id.renameCardDesTxt);

                okRenameCardDes = (Button) dialogDes.findViewById(R.id.okRenameCardDesBtn);

                //when click ok rename
                okRenameCardDes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        thisCard.setDesc(editCardDes.getText().toString());
                        dialogDes.dismiss();
                    }
                });
                dialogDes.show();
            }
        });

        deleteCard = (Button) findViewById(R.id.deleteCardBtn);
        deleteCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Storage.getInstance().loadSavedCard().get(indexList).removeCard(indexCard);
                //Intent intent = new Intent (ShowCard.this,ShowCardList.class);
                finish();
                //startActivity(intent);
            }
        });

        addComment = (Button) findViewById(R.id.addCommentBtn);
        addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //custom dialog
                final Dialog dialogComment = new Dialog(ShowCard.this);
                dialogComment.setContentView(R.layout.comment_card_dialog);
                dialogComment.setTitle("Edit comment");

                //set a dialog
                editComment = (EditText) dialogComment.findViewById(R.id.commentTxt);

                okComment = (Button) dialogComment.findViewById(R.id.okCommentBtn);

                //when click ok rename
                okComment.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        thisCard.getComments().add(new Comment(editComment.getText().toString()));
                        for(int i= 0; i < thisCard.getComments().size(); i++) {
                            Log.e("Comment", thisCard.getComments().get(i).getText());
                        }
                        dialogComment.dismiss();
                    }
                });
                dialogComment.show();
            }
        });


        showComment = (Button) findViewById(R.id.showCommentBtn);
        showComment.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShowCard.this, ShowComment.class);
                intent.putExtra("i",indexList);
                intent.putExtra("j",indexCard);
                startActivity(intent);
            }
        });



    }

}



