package com.example.net.pocketcard.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.net.pocketcard.Adapter.CardAdapter;
import com.example.net.pocketcard.Adapter.CardListAdapter;
import com.example.net.pocketcard.Models.Card;
import com.example.net.pocketcard.Models.CardList;
import com.example.net.pocketcard.Models.Storage;
import com.example.net.pocketcard.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by PrangerZ54 on 3/1/2016 AD.
 */
public class ShowCardList extends AppCompatActivity {


    private CardAdapter cardAdapter;
    private List<Card> cards;
    private ListView cardListView;
    private CardList cardList;

    //Edit text
    private EditText editText;

    //Button
    private Button RenameButton;
    private Button AddCardButton;
    private Button okRenameButton;
    private Button deletCardListButton;

    private int indexList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_collection);
        indexList = getIntent().getIntExtra("i", -1);
        //cards = Storage.getInstance().loadSavedCard().get(i).getNewCard();
        initComponents();
    }

    protected void onStart(){
        super.onStart();
        cardAdapter.notifyDataSetChanged();
        Log.i("hello", Storage.getInstance().loadSavedCard().size() + "");
        cards.clear();
        for(Card thisCard : Storage.getInstance().loadSavedCard().get(indexList).getNewCardList()) {
            cards.add(thisCard);
        }

    }
    private void initComponents() {
        cards = new ArrayList<Card>();
        cardAdapter = new CardAdapter(this,R.layout.card_cell,cards);
        cardListView = (ListView)findViewById(R.id.cardListView);
        cardListView.setAdapter( cardAdapter );
        cardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int indexCard, long l) {
                Intent intent = new Intent(ShowCardList.this, ShowCard.class);
                intent.putExtra("i", indexList);
                intent.putExtra("j", indexCard);
                startActivity(intent);
            }
        });
        RenameButton = (Button)findViewById(R.id.renameListBtn);
        RenameButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                //custom dialog
                final Dialog dialog = new Dialog(ShowCardList.this);
                dialog.setContentView(R.layout.rename_dialog);
                dialog.setTitle("Rename card list");

                //set a dialog
                editText = (EditText) dialog.findViewById(R.id.renameListTxt);

                okRenameButton = (Button) dialog.findViewById(R.id.okRenameBtn);

                //when click ok rename
                okRenameButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Storage.getInstance().loadSavedCard().get(indexList).setCardListTitle(editText.getText().toString());
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }

        });

        AddCardButton = (Button)findViewById(R.id.addCardBtn);
        AddCardButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(ShowCardList.this, NewCard.class);
                intent.putExtra("listNo", indexList);
                startActivity(intent);

            }

        });

        deletCardListButton = (Button) findViewById(R.id.deleteCardListBtn);
        deletCardListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Storage.getInstance().deleteCardList(indexList);
                //Intent intent = new Intent (ShowCard.this,ShowCardList.class);
                finish();
            }
        });




    }
}
